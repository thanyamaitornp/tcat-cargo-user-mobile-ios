*** Settings ***
Documentation     A test suite with a single test for valid login.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.

Library     AppiumLibrary



*** Variables ***
${Name_Tracking}    MTESTROBOT03
${Bill_Tracking}    ATEST-MOBILE02
${Screenshot}       TCAT
${namecouponfree}    name3

*** Keywords ***
TCAT-01-01 Login User
    Wait until element is visible           id=email
    Input Text             id=email     jkrit.sr@gmail.com
    Wait until element is visible           id=password
    Input Text             id=password     123456
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT01/${Screenshot}01-01.png
    Sleep   2s
    Tap          id=submit
    Sleep   5s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT01/${Screenshot}01-02.png
    Sleep   2s

TCAT-02-01 Chat
    Wait until element is visible           accessibility_id=HomeIndex
    Tap           accessibility_id=HomeIndex
    Sleep       2s
    Wait until element is visible           id=grid.ChatBox
    Tap          id=grid.ChatBox
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT02/${Screenshot}02-01.png
    Wait until element is visible           id=message
    Input Text          id=message          สวัสดีค่ะ
    Wait until element is visible           id=file.gallery
    Tap           id=file.gallery
    Sleep       3s
    Tap           chain=**/XCUIElementTypeWindow/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther    
    Sleep       3s
    Tap           chain=**/XCUIElementTypeWindow/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]/XCUIElementTypeOther/XCUIElementTypeImage
    Sleep   2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT02/${Screenshot}02-02.png
    Wait until element is visible           id=submit
    Tap           id=submit
    Sleep       2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT02/${Screenshot}02-03.png
    Wait until element is visible           id=header-back
    Tap            id=header-back
    # Wait until element is visible           chain=**/XCUIElementTypeOther[`label == " กล่องข้อความ"`]
    # Tap           chain=**/XCUIElementTypeOther[`label == " กล่องข้อความ"`]
    Sleep   3s

TCAT-03-01 Tracking Display
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "  หน้าหลัก"`]
    Tap             chain=**/XCUIElementTypeButton[`label == "  หน้าหลัก"`]
    Sleep       2s
    Wait until element is visible           id=grid.Tracking
    Tap           id=grid.Tracking
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT03/${Screenshot}03-01.png
    Sleep   3s

TCAT-03-02 Tracking Add
    Wait until element is visible           accessibility_id=HomeIndex
    Tap           accessibility_id=HomeIndex
    Sleep       2s
    Wait until element is visible           id=grid.Tracking
    Tap           id=grid.Tracking
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT03/${Screenshot}03-02-1.png
    Sleep       2s
    Wait until element is visible           id=TextButton
    Tap           id=TextButton
    Wait until element is visible           id=code
    Input Text        id=code          ${Name_Tracking}
    Sleep     2s
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "-- เลือก ประเภทสินค้า --"`]
    Tap             chain=**/XCUIElementTypeButton[`label == "-- เลือก ประเภทสินค้า --"`]
    Sleep       2s
    Tap             chain=**/XCUIElementTypeButton[`label == "-- เลือก ประเภทสินค้า --"`]

    Tap             id=product_type.car
    Wait until element is visible           id=qty
    Input Text       id=qty         2
    Wait until element is visible           id=product_remark
    Input Text      id=product_remark        test
    Wait until element is visible           id=user_remark 
    Input Text      id=user_remark        test
    Sleep       2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT03/${Screenshot}03-02-2.png
    Wait until element is visible           id=submit
    Tap             id=submit
    Tap             id=submit
    Sleep       2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT03/${Screenshot}03-02-3.png
    Wait until element is visible           id=header-back
    Tap            id=header-back
    Sleep   3s


TCAT-03-03 Tracking Edit 

    Wait until element is visible           accessibility_id=HomeIndex
    Tap           accessibility_id=HomeIndex
    Sleep       2s
    Wait until element is visible           id=grid.Tracking
    Tap           id=grid.Tracking
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT03/${Screenshot}03-03-1.png
    Wait until element is visible           id=tracking.item.${Name_Tracking}
    Tap           id=tracking.item.${Name_Tracking}
    Sleep       2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT03/${Screenshot}03-03-2.png
    Sleep       2s
    Scroll Down      chain=**/XCUIElementTypeOther[`label == "Tracking"`][1]
    Sleep       2s
    Wait until element is visible           id=navigateEdit
    Tap           id=navigateEdit
    Sleep       2s
    # Press Key       chain=**/XCUIElementTypeOther[`label == "ประเภทสินค้า *"`][1]/XCUIElementTypeOther[2]/XCUIElementTypeButton       
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT03/${Screenshot}03-03-3.png
    Sleep       2s
    Input Text       id=qty      3
    Input Text       id=product_remark     test1   
    Input Text      id=user_remark        test1
    Sleep       2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT03/${Screenshot}03-03-4.png
    Sleep       2s
    Wait until element is visible           id=submit
    Tap             id=submit
    Tap             id=submit
    Sleep   3s

TCAT-03-04 Tracking Edit Remark
    Wait until element is visible           accessibility_id=HomeIndex
    Tap           accessibility_id=HomeIndex
    Sleep       3s
    Wait until element is visible           id=grid.Tracking
    Tap           id=grid.Tracking
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT03/${Screenshot}03-04-5.png
    Wait until element is visible           id=tracking.item.${Name_Tracking}
    Tap           id=tracking.item.${Name_Tracking}
    Sleep       2s
    Scroll Down      chain=**/XCUIElementTypeOther[`label == "Tracking"`][1]
    Wait until element is visible           id=user_remark
    Input Text           id=user_remark         2
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT03/${Screenshot}03-04-6.png
    Sleep       2s
    Tap        chain=**/XCUIElementTypeOther[`label == "ค่าลังไม้(หยวน)"`][2]   
    Scroll Down      chain=**/XCUIElementTypeOther[`label == "Tracking"`][1]
    Sleep       2s
    Wait until element is visible           id=submit
    Tap           id=submit
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT03/${Screenshot}03-04-7.png
    Sleep       3s

TCAT-03-05 Tracking Delete
    Wait until element is visible           accessibility_id=HomeIndex
    Tap           accessibility_id=HomeIndex
    Sleep       2s
    Wait until element is visible           id=grid.Tracking
    Tap           id=grid.Tracking
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT03/${Screenshot}03-05-1.png
    Wait until element is visible           id=tracking.item.${Name_Tracking}
    Tap           id=tracking.item.${Name_Tracking}
    Sleep       2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT03/${Screenshot}03-05-2.png
    Wait until element is visible           id=onDelete
    Tap           id=onDelete
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT03/${Screenshot}03-05-3.png
    Wait until element is visible           id=ตกลง
    Tap           id=ตกลง
    
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT03/${Screenshot}03-05-4.png
    Sleep       3s

TCAT-04-01 Bill Display
    Wait until element is visible           accessibility_id=HomeIndex
    Tap           accessibility_id=HomeIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-01-1.png
    Sleep       3s
    Wait until element is visible           id=grid.Bill
    Tap             id=grid.Bill
    Sleep   2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-01-2.png
    Wait until element is visible           id=tab.0
    Tap             id=tab.0
    Sleep   2s
    Wait until element is visible           id=tab.1
    Tap             id=tab.1
    Sleep   2s
    Wait until element is visible           id=tab.2
    Tap             id=tab.2
    Sleep   2s
    Wait until element is visible           id=tab.3
    Tap             id=tab.3
    Sleep   2s
    Wait until element is visible           id=tab.4
    Tap             id=tab.4
    Sleep   2s
    Wait until element is visible           id=header-back
    Tap            id=header-back
    Sleep   3s


TCAT-04-02 Bill Add
    Wait until element is visible           accessibility_id=HomeIndex
    Tap           accessibility_id=HomeIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-02-1.png
    Sleep       3s
    Wait until element is visible           id=grid.Bill
    Tap             id=grid.Bill
    Sleep   2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-02-2.png
    Wait until element is visible           id=TextButton
    Tap             id=TextButton
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-02-3.png
    Wait until element is visible           id=tracking.item.${Bill_Tracking}
    Tap             id=tracking.item.${Bill_Tracking}
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-02-4.png
    Sleep       3s
    # Scroll Down     chain=**/XCUIElementTypeOther[`label == "Vertical scroll bar, 1 page"`]
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-02-5.png
    Wait until element is visible           id=submit
    Tap             id=submit
    Sleep   2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-02-6.png
    Wait until element is visible           chain=**/XCUIElementTypeOther[`label == "การขนส่งภายในประเทศ *"`][1]/XCUIElementTypeOther[2]/XCUIElementTypeButton
    Tap             chain=**/XCUIElementTypeOther[`label == "การขนส่งภายในประเทศ *"`][1]/XCUIElementTypeOther[2]/XCUIElementTypeButton
    Wait until element is visible           id=thai_shipping_id.2
    Tap             id=thai_shipping_id.2
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "-- เลือก ที่อยู่จัดส่งสินค้า --"`]
    Tap             chain=**/XCUIElementTypeButton[`label == "-- เลือก ที่อยู่จัดส่งสินค้า --"`]
    Wait until element is visible           id=user_address_id.33369
    Tap             id=user_address_id.33369
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-02-7.png
    Wait until element is visible           id=submit
    Tap             id=submit
    Sleep   2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-02-8.png
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "-- เลือกคูปอง --"`]
    Tap             chain=**/XCUIElementTypeButton[`label == "-- เลือกคูปอง --"`]
    Wait until element is visible           id=coupon_code.select.0
    Tap             id=coupon_code.select.0
    Sleep   2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-02-9.png
    Wait until element is visible           id=user_remark
    Input Text             id=user_remark      test
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-02-10.png
    Wait until element is visible           id=onCouponCheck
    Tap             id=onCouponCheck
    Wait until element is visible           id=submit
    Tap             id=submit
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-02-11.png
    Sleep   30s

    

TCAT-04-03 Bill Cancel
    Wait until element is visible           accessibility_id=HomeIndex
    Tap           accessibility_id=HomeIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-03-1.png
    Sleep       3s
    Wait until element is visible           id=grid.Bill
    Tap             id=grid.Bill
    Sleep   2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-03-2.png
    Wait until element is visible           id=bill.item.476067
    Tap             id=bill.item.476067
    Sleep   2s
    Scroll Down     chain=**/XCUIElementTypeOther[`label == "การขนส่ง การขนส่งในประเทศ ชื่อผู้รับ โทรศัพท์ ที่อยู่จัดส่งสินค้า"`]
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-03-3.png
    Sleep   2s
    Wait until element is visible           id=onDelete
    Tap             id=onDelete
    Sleep       2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-03-4.png
    # Wait until element is visible           id=ยกเลิก
    # Tap           id=ยกเลิก
    Wait until element is visible           id=ตกลง
    Tap           id=ตกลง
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-03-5.png
    Sleep       3s


TCAT-04-04 Bill Payment
    Wait until element is visible           accessibility_id=HomeIndex
    Tap           accessibility_id=HomeIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-04-1.png
    Sleep       3s
    Wait until element is visible           id=grid.Bill
    Tap             id=grid.Bill
    Sleep   2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-04-2.png
    Wait until element is visible           id=tab.2
    Tap             id=tab.2
    Sleep   2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-04-3.png
    Wait until element is visible           id=bill.item.476064
    Tap             id=bill.item.476064
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-04-4.png
    Sleep       2s
    Scroll Down     chain=**/XCUIElementTypeOther[`label == "Vertical scroll bar, 2 pages"`]
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-04-5.png
    Wait until element is visible           id=onPayment
    Tap             id=onPayment
    Sleep       2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-04-6.png
    # Wait until element is visible           id=ยกเลิก
    # Tap           id=ยกเลิก
    Wait until element is visible           id=ชำระบิล
    Tap           id=ชำระบิล
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-04-7.png
    Sleep       3s
    

TCAT-04-05 Bill Edit Remark
    Wait until element is visible           accessibility_id=HomeIndex
    Tap           accessibility_id=HomeIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-05-1.png
    Sleep       3s
    Wait until element is visible           id=grid.Bill
    Tap             id=grid.Bill
    Sleep   2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-05-2.png
    Wait until element is visible           id=bill.item.476053
    Tap             id=bill.item.476053
    Sleep   2s
    Scroll Down     chain=**/XCUIElementTypeOther[`label == "Vertical scroll bar, 2 pages"`]
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-05-3.png
    Wait until element is visible           id=user_remark
    Input Text             id=user_remark           t2
    Wait until element is visible           id=Tracking
    Tap             id=Tracking
    Scroll Down     chain=**/XCUIElementTypeOther[`label == "Vertical scroll bar, 2 pages"`]
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-05-4.png
    Wait until element is visible           id=submit
    Tap             id=submit
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT04/${Screenshot}04-05-5.png
    Sleep   3s
    

TCAT-05-01 Topup Display
    Wait until element is visible           accessibility_id=HomeIndex
    Tap           accessibility_id=HomeIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT05/${Screenshot}05-01-1.png
    Sleep       3s
    Wait until element is visible           id=grid.TopUp
    Tap             id=grid.TopUp
    Sleep   2s
    Wait until element is visible           id=tab.approved
    Tap             id=tab.approved
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT05/${Screenshot}05-01-2.png

    Wait until element is visible            id=topUp.item.356686
    Click element              id=topUp.item.356686
    Sleep   3s
    Wait until element is visible           id=header-back
    Tap           id=header-back
    Wait until element is visible           id=tab.cancel
    Tap           id=tab.cancel
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT05/${Screenshot}05-01-3.png
    Wait until element is visible           	id=topUp.item.356679
    Tap             	id=topUp.item.356679
    Sleep   3s
    Wait until element is visible           id=header-back
    Tap           id=header-back
    Sleep   3s

TCAT-05-02 Topup Add
    Wait until element is visible           accessibility_id=HomeIndex
    Tap           accessibility_id=HomeIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT05/${Screenshot}05-02-1.png
    Sleep       3s
    Wait until element is visible           id=grid.TopUp
    Tap             id=grid.TopUp
    Sleep   2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT05/${Screenshot}05-02-2.png
    Wait until element is visible           id=TextButton
    Tap             id=TextButton
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT05/${Screenshot}05-02-3.png
    Wait until element is visible           id=amount
    Input Text          id=amount            100
    Sleep   2s
    Tap             chain=**/XCUIElementTypeOther[`label == "เครดิตคงเหลือ(บาท)"`][2]
    Sleep   2s
    Tap             chain=**/XCUIElementTypeButton[`label == "-- เลือก ช่องทางการเติม --"`]
    Wait until element is visible           id=method.transfer
    Tap             id=method.transfer
    Wait until element is visible       chain=**/XCUIElementTypeButton[`label == "-- เลือก บัญชีผู้รับ --"`]
    Tap             chain=**/XCUIElementTypeButton[`label == "-- เลือก บัญชีผู้รับ --"`]
    Sleep   2s
    Tap             chain=**/XCUIElementTypeButton[`label == "-- เลือก บัญชีผู้รับ --"`]
    Wait until element is visible           id=system_bank_account_id.3201
    Tap             id=system_bank_account_id.3201
    Scroll Down     id=amount
    Tap             id=date
    Sleep   2s
    Tap             id=date
    Wait until element is visible       id=Confirm
    Tap             id=Confirm
    Sleep   2s
    Tap             id=time
    Wait until element is visible       id=Confirm
    Tap             id=Confirm
    Sleep   2s
    Scroll Down     id=amount
    Wait until element is visible       id=file.gallery
    Tap             id=file.gallery
    Sleep   2s
    Tap           chain=**/XCUIElementTypeWindow/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther    
    Sleep       3s
    Tap           chain=**/XCUIElementTypeWindow/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]/XCUIElementTypeOther/XCUIElementTypeImage
    Sleep   2s
    Input Text          id=user_remark          test
    Sleep   2s
    Tap             chain=**/XCUIElementTypeOther[`label == "วันที่ *"`]
    Scroll Down     id=amount
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT05/${Screenshot}05-02-4.png
    Wait until element is visible       id=submit
    Tap             id=submit
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT05/${Screenshot}05-02-5.png
    Sleep       3s





TCAT-06-01 Withdraw Display

    Wait until element is visible           accessibility_id=HomeIndex
    Tap           accessibility_id=HomeIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT06/${Screenshot}06-01-1.png
    Sleep       3s
    Tap             chain=**/XCUIElementTypeButton[`label == "  หน้าหลัก"`]
    Wait until element is visible           id=grid.Withdraw
    Tap             id=grid.Withdraw
    Sleep   2s
    Wait until element is visible           id=tab.approved
    Tap             id=tab.approved
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT06/${Screenshot}06-01-2.png


    Wait until element is visible            id=withdraw.item.5122
    Click element              id=withdraw.item.5122
    Sleep   3s
    Wait until element is visible           id=header-back
    Tap           id=header-back
    Wait until element is visible           id=tab.cancel
    Tap           id=tab.cancel
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT06/${Screenshot}06-01-3.png

    Wait until element is visible           	id=withdraw.item.5117
    Tap             	id=withdraw.item.5117
    Sleep   3s
    Wait until element is visible           id=header-back
    Tap           id=header-back
    Sleep   3s


TCAT-06-02 Withdraw Add
    Wait until element is visible           accessibility_id=HomeIndex
    Tap           accessibility_id=HomeIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT06/${Screenshot}06-02-1.png
    Sleep       3s
    Wait until element is visible           id=grid.Withdraw
    Tap             id=grid.Withdraw
    Sleep   2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT06/${Screenshot}06-02-2.png

    Wait until element is visible           id=TextButton
    Tap             id=TextButton
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT06/${Screenshot}06-02-3.png
    
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "-- เลือก รหัสธนาคาร ลูกค้า --"`]
    Tap                     chain=**/XCUIElementTypeButton[`label == "-- เลือก รหัสธนาคาร ลูกค้า --"`]
    Wait until element is visible           id=user_bank_account_id.82
    Tap                     id=user_bank_account_id.82
    Sleep       2s
    Input Text      id=amount           99
    Input Text      id=user_remark    test
    Tap              xpath=//XCUIElementTypeStaticText[@name="หมายเหตุ"]
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT06/${Screenshot}06-02-4.png

    Wait until element is visible           id=submit
    Tap                     id=submit
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT06/${Screenshot}06-02-5.png
    Sleep       3s



TCAT-07-01 Statement Display
    Wait until element is visible           accessibility_id=HomeIndex
    Tap           accessibility_id=HomeIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT07/${Screenshot}07-01-1.png
    Sleep       2s
    Wait until element is visible           id=grid.Statement
    Tap           id=grid.Statement
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT07/${Screenshot}07-01-2.png
    Sleep       3s
    Wait until element is visible           id=statement.item.819967
    Tap           id=statement.item.819967
    Sleep       3s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT07/${Screenshot}07-01-3.png
    Wait until element is visible            id=IconButton
    Tap            id=IconButton
    Sleep       5s
    Wait until element is visible           id=header-back
    Tap           id=header-back
    Wait until element is visible           id=header-back
    Tap           id=header-back
    Sleep       3s

TCAT-08-01 Address Add
    Wait until element is visible           accessibility_id=HomeIndex
    Tap           accessibility_id=HomeIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT08/${Screenshot}08-01-1.png
    Sleep       2s
    Wait until element is visible           id=grid.Address
    Tap           id=grid.Address
    Sleep       3s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT08/${Screenshot}08-01-2.png
    Wait until element is visible           id=TextButton
    Tap           id=TextButton
    Wait until element is visible           id=name         
    Input Text              id=name            MOBILE_ADD
    Wait until element is visible           id=tel
    Input Text              id=tel              0934567891
    Sleep         2s
    # Wait until element is visible           id=address
    Input Text            address         13/34 ttest road, 
    Sleep         2s
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "-- เลือกจังหวัด --"`]
    Tap           chain=**/XCUIElementTypeButton[`label == "-- เลือกจังหวัด --"`]
    Sleep         2s
    Tap           chain=**/XCUIElementTypeButton[`label == "-- เลือกจังหวัด --"`]

    Wait until element is visible           id=province_id.1
    Tap           id=province_id.1
    Sleep         3s
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "-- เลือกอำเภอ --"`]
    Tap           chain=**/XCUIElementTypeButton[`label == "-- เลือกอำเภอ --"`]
    Sleep         2s
    Tap           chain=**/XCUIElementTypeButton[`label == "-- เลือกอำเภอ --"`]
    Wait until element is visible           id=amphur_id.33
    Tap           id=amphur_id.33    
    Sleep         3s
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "-- เลือกตำบล --"`]
    Tap           chain=**/XCUIElementTypeButton[`label == "-- เลือกตำบล --"`]
    Sleep         2s
    Tap           chain=**/XCUIElementTypeButton[`label == "-- เลือกตำบล --"`]
    Wait until element is visible           id=district_code.103302
    Tap           id=district_code.103302
    Sleep         3s
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "-- เลือก สถานะ --"`]
    Tap           chain=**/XCUIElementTypeButton[`label == "-- เลือก สถานะ --"`]
    Sleep         2s
    Tap           chain=**/XCUIElementTypeButton[`label == "-- เลือก สถานะ --"`]
    Wait until element is visible           id=status.inactive
    Tap           id=status.active
    Scroll Down     id=address
    Wait until element is visible           id=user_remark
    Input Text              id=user_remark              test
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT08/${Screenshot}08-01-3.png
    Tap        id=สถานะ *
    Scroll Down     id=address
    Wait until element is visible           id=submit
    Tap           id=submit
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT08/${Screenshot}08-01-4.png
    Sleep       3s


TCAT-08-02 Address Edit
    Wait until element is visible           accessibility_id=HomeIndex
    Tap           accessibility_id=HomeIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT08/${Screenshot}08-02-1.png
    Sleep       3s
    Wait until element is visible           id=grid.Address
    Tap           id=grid.Address
    Sleep       3s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT08/${Screenshot}08-02-2.png
    Wait until element is visible           id=address.item.75143
    Tap           id=address.item.75143
    Sleep       2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT08/${Screenshot}08-02-3.png
    Wait until element is visible           chain=**/XCUIElementTypeOther[`label == "สถานะ *"`][1]/XCUIElementTypeOther[2]/XCUIElementTypeButton
    Tap           chain=**/XCUIElementTypeOther[`label == "สถานะ *"`][1]/XCUIElementTypeOther[2]/XCUIElementTypeButton
    Sleep         2s
    Tap           chain=**/XCUIElementTypeOther[`label == "สถานะ *"`][1]/XCUIElementTypeOther[2]/XCUIElementTypeButton
    Wait until element is visible           id=status.active
    Tap           id=status.active
    Scroll Down     id=address
    Wait until element is visible           id=user_remark
    Input Text              id=user_remark              22
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT08/${Screenshot}08-02-4.png
    Tap        id=สถานะ *
    Scroll Down     id=address
    Wait until element is visible           id=submit
    Tap           id=submit
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT08/${Screenshot}08-02-5.png
    Sleep       3s


TCAT-09-01 NoCode Search
    Wait until element is visible           accessibility_id=HomeIndex
    Tap           accessibility_id=HomeIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT09/${Screenshot}09-01-1.png
    Sleep       3s
    Wait until element is visible           id=grid.NoCode
    Tap           id=grid.NoCode
    Sleep       3s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT09/${Screenshot}09-01-2.png
    Wait until element is visible           id=IconButton
    Tap             id=IconButton
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT09/${Screenshot}09-01-3.png
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "-- เลือก ประเภท --"`]
    Tap             chain=**/XCUIElementTypeButton[`label == "-- เลือก ประเภท --"`]
    Sleep       1s
    Tap             chain=**/XCUIElementTypeButton[`label == "-- เลือก ประเภท --"`]
    Wait until element is visible           id=type.bag
    Tap                 id=type.bag
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT09/${Screenshot}09-01-4.png
    Wait until element is visible           id=submit
    Tap                 id=submit
    Sleep       5s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT09/${Screenshot}09-01-5.png
    Wait until element is visible           id=header-back
    Tap           id=header-back
    Sleep       3s


TCAT-10-01 Contact Display
    Wait until element is visible           accessibility_id=HomeIndex
    Tap           accessibility_id=HomeIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT10/${Screenshot}10-01-1.png
    Sleep       3s
    Wait until element is visible           id=grid.Contact
    Tap           id=grid.Contact
    Sleep       3s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT10/${Screenshot}10-01-2.png
    Wait until element is visible           id=header-back
    Tap           id=header-back
    Sleep       3s
    

TCAT-11-01 News and Promotion Display
    Wait until element is visible           accessibility_id=NewsIndex
    Tap           accessibility_id=NewsIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT11/${Screenshot}11-01-1.png
    Sleep       3s
    Wait until element is visible           id=news.items.1756
    Tap             id=news.items.1756
    Sleep       2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT11/${Screenshot}11-01-2.png
    Wait until element is visible           id=header-back
    Tap           id=header-back
    Sleep       3s


TCAT-12-01 Account Edit
    Wait until element is visible           accessibility_id=AccountIndex
    Tap           accessibility_id=AccountIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT12/${Screenshot}12-01-1.png
    Sleep       3s
    Wait until element is visible           id=info
    Tap           id=info
    Sleep       2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT12/${Screenshot}12-01-2.png
    Wait until element is visible           id=birthday
    Tap           id=birthday
    Sleep       2s
    Wait until element is visible           id=Confirm
    Tap           id=Confirm
    Scroll Down     id=birthday
    Wait until element is visible           chain=**/XCUIElementTypeOther[`label == "SMS แจ้งเตือน *"`][1]/XCUIElementTypeOther[2]/XCUIElementTypeButton
    Tap           chain=**/XCUIElementTypeOther[`label == "SMS แจ้งเตือน *"`][1]/XCUIElementTypeOther[2]/XCUIElementTypeButton
    Wait until element is visible           id=sms_notification.inactive
    Tap           id=sms_notification.inactive
    Wait until element is visible           id=user_remark
    Input Text              id=user_remark              1
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT12/${Screenshot}12-01-3.png
    Tap           chain=**/XCUIElementTypeOther[`label == "SMS แจ้งเตือน *"`][1]/XCUIElementTypeOther[2]/XCUIElementTypeButton
    Scroll Down     id=birthday
    Wait until element is visible           id=submit
    Tap                 id=submit
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT12/${Screenshot}12-01-4.png
    Sleep       3s






TCAT-13-01 Address Add
    Wait until element is visible           accessibility_id=AccountIndex
    Tap           accessibility_id=AccountIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT13/${Screenshot}13-01-1.png
    Sleep       3s
    Wait until element is visible           id=location-pin
    Tap           id=location-pin
    Sleep       3s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT13/${Screenshot}13-01-2.png
    Wait until element is visible           id=TextButton
    Tap           id=TextButton
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT13/${Screenshot}13-01-3.png
    Wait until element is visible           id=name         
    Input Text              id=name            MOBILE_ADD2
    Wait until element is visible           id=tel
    Input Text              id=tel              0934567891
    Sleep         2s
    # Wait until element is visible           id=address
    Input Text            address         13/34 ttest road, 
    Sleep         2s
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "-- เลือกจังหวัด --"`]
    Tap           chain=**/XCUIElementTypeButton[`label == "-- เลือกจังหวัด --"`]
    Sleep         2s
    Tap           chain=**/XCUIElementTypeButton[`label == "-- เลือกจังหวัด --"`]

    Wait until element is visible           id=province_id.1
    Tap           id=province_id.1
    Sleep         3s
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "-- เลือกอำเภอ --"`]
    Tap           chain=**/XCUIElementTypeButton[`label == "-- เลือกอำเภอ --"`]
    Sleep         2s
    Tap           chain=**/XCUIElementTypeButton[`label == "-- เลือกอำเภอ --"`]
    Wait until element is visible           id=amphur_id.33
    Tap           id=amphur_id.33    
    Sleep         3s
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "-- เลือกตำบล --"`]
    Tap           chain=**/XCUIElementTypeButton[`label == "-- เลือกตำบล --"`]
    Sleep         2s
    Tap           chain=**/XCUIElementTypeButton[`label == "-- เลือกตำบล --"`]
    Wait until element is visible           id=district_code.103302
    Tap           id=district_code.103302
    Sleep         3s
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "-- เลือก สถานะ --"`]
    Tap           chain=**/XCUIElementTypeButton[`label == "-- เลือก สถานะ --"`]
    Sleep         2s
    Tap           chain=**/XCUIElementTypeButton[`label == "-- เลือก สถานะ --"`]
    Wait until element is visible           id=status.inactive
    Tap           id=status.active
    Scroll Down     id=address
    Wait until element is visible           id=user_remark
    Input Text              id=user_remark              test
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT13/${Screenshot}13-01-4.png
    Tap        id=สถานะ *
    Scroll Down     id=address
    Wait until element is visible           id=submit
    Tap           id=submit
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT13/${Screenshot}13-01-5.png
    Sleep       3s

TCAT-13-02 Address Edit
    Wait until element is visible           accessibility_id=AccountIndex
    Tap           accessibility_id=AccountIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT13/${Screenshot}13-02-1.png
    Sleep       2s
    Wait until element is visible           id=location-pin
    Tap           id=location-pin
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT13/${Screenshot}13-02-2.png
    Sleep       3s
    Wait until element is visible           id=address.item.75143
    Tap           id=address.item.75143
    Sleep       2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT13/${Screenshot}13-02-3.png
    Wait until element is visible           chain=**/XCUIElementTypeOther[`label == "สถานะ *"`][1]/XCUIElementTypeOther[2]/XCUIElementTypeButton
    Tap           chain=**/XCUIElementTypeOther[`label == "สถานะ *"`][1]/XCUIElementTypeOther[2]/XCUIElementTypeButton
    Sleep         2s
    Tap           chain=**/XCUIElementTypeOther[`label == "สถานะ *"`][1]/XCUIElementTypeOther[2]/XCUIElementTypeButton
    Wait until element is visible           id=status.active
    Tap           id=status.active
    Scroll Down     id=address
    Wait until element is visible           id=user_remark
    Input Text              id=user_remark              2
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT13/${Screenshot}13-02-4.png
    Tap        id=สถานะ *
    Scroll Down     id=address
    Wait until element is visible           id=submit
    Tap           id=submit
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT13/${Screenshot}13-02-5.png
    Sleep       3s


TCAT-14-01 Bank Add
    Wait until element is visible           accessibility_id=AccountIndex
    Tap           accessibility_id=AccountIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT14/${Screenshot}14-01-1.png
    Sleep       2s
    Wait until element is visible           id=bank
    Tap           id=bank
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT14/${Screenshot}14-01-2.png
    Wait until element is visible           id=TextButton
    Tap           id=TextButton
    Sleep       2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT14/${Screenshot}14-01-3.png
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "-- เลือก ธนาคาร --"`]
    Tap           chain=**/XCUIElementTypeButton[`label == "-- เลือก ธนาคาร --"`]
    Sleep       2s
    Tap           chain=**/XCUIElementTypeButton[`label == "-- เลือก ธนาคาร --"`]
    Wait until element is visible           id=bank.SCB
    Tap           id=bank.SCB
    Wait until element is visible           id=branch
    Input Text      id=branch           SRIRACHA
    Wait until element is visible           id=title
    Input Text      id=title                MOBILE_TEST
    Wait until element is visible           id=account
    Input Text      id=account          7583838923
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "-- เลือก สถานะ --"`]
    Tap           chain=**/XCUIElementTypeButton[`label == "-- เลือก สถานะ --"`]
    Sleep       2s
    Tap           chain=**/XCUIElementTypeButton[`label == "-- เลือก สถานะ --"`]
    Wait until element is visible           id=status.active
    Tap           id=status.active
    Wait until element is visible           id=user_remark
    Input Text      id=user_remark          test
    Sleep       2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT14/${Screenshot}14-01-4.png
    Wait until element is visible      chain=**/XCUIElementTypeOther[`label == "หมายเหตุ"`][2]    
    Tap           chain=**/XCUIElementTypeOther[`label == "หมายเหตุ"`][2]
    Wait until element is visible           id=submit
    Tap           id=submit
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT14/${Screenshot}14-01-5.png
    Sleep       3s



TCAT-14-02 Bank Edit
    Wait until element is visible           accessibility_id=AccountIndex
    Tap           accessibility_id=AccountIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT14/${Screenshot}14-02-1.png
    Sleep       2s
    Wait until element is visible           id=bank
    Tap           id=bank
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT14/${Screenshot}14-02-2.png
    Wait until element is visible           id=bank.item.7975
    Tap           id=bank.item.7975
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT14/${Screenshot}14-02-3.png
    Wait until element is visible           id=title
    Input Text      id=title            12
    Wait until element is visible           id=user_remark
    Input Text      id=user_remark          qq
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT14/${Screenshot}14-02-4.png
    Wait until element is visible      chain=**/XCUIElementTypeOther[`label == "หมายเหตุ"`][2]    
    Tap           chain=**/XCUIElementTypeOther[`label == "หมายเหตุ"`][2]
    Wait until element is visible           id=submit
    Tap           id=submit
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT14/${Screenshot}14-02-5.png
    Sleep       3s


TCAT-15-01 PointOrder Display
    Wait until element is visible           accessibility_id=AccountIndex
    Tap           accessibility_id=AccountIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT15/${Screenshot}15-01-1.png
    Sleep       2s
    Wait until element is visible           id=gift
    Tap           id=gift
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT15/${Screenshot}15-01-2.png
    Wait until element is visible           id=header-back
    Tap           id=header-back
    Sleep       3s


TCAT-15-02 PointOrder Add
    Wait until element is visible           accessibility_id=AccountIndex
    Tap           accessibility_id=AccountIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT15/${Screenshot}15-02-1.png
    Sleep       2s
    Wait until element is visible           id=gift
    Tap           id=gift
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT15/${Screenshot}15-02-2.png
    Wait until element is visible           id=TextButton
    Tap           id=TextButton
    Sleep   2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT15/${Screenshot}15-02-3.png
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "เพิ่ม"`][1]
    Tap           chain=**/XCUIElementTypeButton[`label == "เพิ่ม"`][1]
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "ถัดไป"`]
    Tap           chain=**/XCUIElementTypeButton[`label == "ถัดไป"`]
    Sleep   2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT15/${Screenshot}15-02-4.png
    Wait until element is visible           chain=**/XCUIElementTypeOther[`label == "การขนส่งภายในประเทศ *"`][1]/XCUIElementTypeOther[2]/XCUIElementTypeButton
    Tap           chain=**/XCUIElementTypeOther[`label == "การขนส่งภายในประเทศ *"`][1]/XCUIElementTypeOther[2]/XCUIElementTypeButton
    Sleep       2s
    Wait until element is visible          id=shipping_method_id.2
    Tap           id=shipping_method_id.2
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "-- เลือก ที่อยู่จัดส่งสินค้า --"`]
    Tap           chain=**/XCUIElementTypeButton[`label == "-- เลือก ที่อยู่จัดส่งสินค้า --"`]
    Wait until element is visible          id=address_id.33483
    Tap           id=address_id.33483
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT15/${Screenshot}15-02-5.png
    Wait until element is visible           id=submit
    Tap           id=submit
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT15/${Screenshot}15-02-6.png
    Sleep       3s
    Input Text      id=user_remark          test
    Wait until element is visible      chain=**/XCUIElementTypeStaticText[`label == "หมายเหตุ"`]    
    Tap           chain=**/XCUIElementTypeStaticText[`label == "หมายเหตุ"`]
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT15/${Screenshot}15-02-7.png
    Wait until element is visible           id=submit
    Tap           id=submit
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT15/${Screenshot}15-02-8.png
    Sleep       3s



TCAT-15-03 PointOrder Edit Remark
    Wait until element is visible           accessibility_id=AccountIndex
    Tap           accessibility_id=AccountIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT15/${Screenshot}15-03-1.png
    Sleep       3s
    Wait until element is visible           id=gift
    Tap           id=gift
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT15/${Screenshot}15-03-2.png
    Wait until element is visible           id=pointOrder.item.1333
    Tap           id=pointOrder.item.1333
    Sleep       2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT15/${Screenshot}15-03-3.png
    Scroll Down      chain=**/XCUIElementTypeOther[`label == "Vertical scroll bar, 2 pages"`]
    Input Text       id=user_remark          test
    Wait until element is visible      chain=**/XCUIElementTypeStaticText[`label == "หมายเหตุ"`]    
    Tap           chain=**/XCUIElementTypeStaticText[`label == "หมายเหตุ"`]
    Sleep       2s
    Scroll Down      chain=**/XCUIElementTypeOther[`label == "Vertical scroll bar, 2 pages"`]
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT15/${Screenshot}15-03-4.png
    Wait until element is visible           id=submit
    Tap           id=submit
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT15/${Screenshot}15-03-5.png
    Sleep       3s    


TCAT-15-04 PointOrder Cancel

    Wait until element is visible           accessibility_id=AccountIndex
    Tap           accessibility_id=AccountIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT15/${Screenshot}15-04-1.png
    Sleep       3s
    Wait until element is visible           id=gift
    Tap           id=gift
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT15/${Screenshot}15-04-2.png
    Wait until element is visible           id=pointOrder.item.1343
    Tap           id=pointOrder.item.1343
    Sleep       2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT15/${Screenshot}15-04-3.png
    Wait until element is visible           id=onDelete
    Tap           id=onDelete
    Sleep       2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT15/${Screenshot}15-04-4.png
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "ยกเลิก"`]
    # Tap           chain=**/XCUIElementTypeButton[`label == "ยกเลิก"`]  
    Tap           chain=**/XCUIElementTypeButton[`label == "ตกลง"`]
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT15/${Screenshot}15-04-5.png
    Sleep       3s  
# **/XCUIElementTypeButton[`label == "ตกลง"`]

TCAT-15-05 PointOrder Payment
    Wait until element is visible           accessibility_id=AccountIndex
    Tap           accessibility_id=AccountIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT15/${Screenshot}15-05-1.png
    Sleep       3s
    Wait until element is visible           id=gift
    Tap           id=gift
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT15/${Screenshot}15-05-2.png
    Wait until element is visible           id=pointOrder.item.1328
    Tap           id=pointOrder.item.1328
    Sleep       2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT15/${Screenshot}15-05-3.png
    Scroll Down      chain=**/XCUIElementTypeOther[`label == "รายการ จำนวนเงิน(บาท) ค่าขนส่งภายในประเทศ 74.00 ยอดรวม 74.00 ยอดที่ต้องชำระ 74.00"`][1]
    Wait until element is visible           id=onPayment
    Tap           id=onPayment
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT15/${Screenshot}15-05-4.png
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "ยกเลิก"`]
    # Tap           chain=**/XCUIElementTypeButton[`label == "ยกเลิก"`]  
    Tap           chain=**/XCUIElementTypeButton[`label == "ชำระบิล"`]  
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT15/${Screenshot}15-05-5.png
    Sleep       3s  
# chain=**/XCUIElementTypeButton[`label == "ชำระบิล"`]  




TCAT-22-01 Coupon Add

    Wait until element is visible           accessibility_id=AccountIndex
    Tap           accessibility_id=AccountIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT22/${Screenshot}22-01-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=ticket-confirmation
    Tap           accessibility_id=ticket-confirmation
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT22/${Screenshot}22-01-2.png
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "TextButton"`]
    Tap           chain=**/XCUIElementTypeButton[`label == "TextButton"`]
    Sleep       2s
    Tap           chain=**/XCUIElementTypeButton[`label == "onIncrease"`][1]
    Sleep       2s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT22/${Screenshot}22-01-3.png
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "ถัดไป"`]
    Tap           chain=**/XCUIElementTypeButton[`label == "ถัดไป"`]
    Sleep       3s 
    Wait until element is visible           chain=**/XCUIElementTypeTextField[`label == "user_remark"`]
    Input Text           chain=**/XCUIElementTypeTextField[`label == "user_remark"`]            Test
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT22/${Screenshot}22-01-4.png
    Wait until element is visible           accessibility_id=submit
    Tap         accessibility_id=submit
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT22/${Screenshot}22-01-5.png
    Sleep       3s 



TCAT-22-02 Coupon Edit Remark
    Wait until element is visible           accessibility_id=AccountIndex
    Tap           accessibility_id=AccountIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT22/${Screenshot}22-02-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=ticket-confirmation
    Tap           accessibility_id=ticket-confirmation
    Sleep       3s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT22/${Screenshot}22-02-2.png
    Wait until element is visible           chain=**/XCUIElementTypeOther[`label == "billCoupon.item.1073"`][1]
    Tap                 chain=**/XCUIElementTypeOther[`label == "billCoupon.item.1073"`][1]
    Wait until element is visible           chain=**/XCUIElementTypeTextField[`label == "user_remark"`]
    Input Text        chain=**/XCUIElementTypeTextField[`label == "user_remark"`]         TEST
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT22/${Screenshot}22-02-3.png
    Sleep       3s
    Tap             chain=**/XCUIElementTypeStaticText[`label == "รายการที่ซื้อ"`]
    Wait until element is visible           accessibility_id=submit
    Tap       accessibility_id=submit
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT22/${Screenshot}22-02-4.png
    Sleep   2s
    Tap         accessibility_id=header-back
    Sleep       3s



TCAT-22-03 Coupon Payment
    Wait until element is visible           accessibility_id=AccountIndex
    Tap           accessibility_id=AccountIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT22/${Screenshot}22-03-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=ticket-confirmation
    Tap           accessibility_id=ticket-confirmation
    Sleep       3s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT22/${Screenshot}22-03-2.png
    Wait until element is visible           chain=**/XCUIElementTypeOther[`label == "billCoupon.item.1074"`][2]
    Tap                 chain=**/XCUIElementTypeOther[`label == "billCoupon.item.1074"`][2]
    Wait until element is visible           accessibility_id=onPayment
    Tap        accessibility_id=onPayment
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT22/${Screenshot}22-03-3.png
    Sleep       3s
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "ชำระบิล"`]
    Tap        chain=**/XCUIElementTypeButton[`label == "ชำระบิล"`]
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT22/${Screenshot}22-03-4.png
    Sleep       3s


TCAT-22-04 Coupon Delete
    Wait until element is visible           accessibility_id=AccountIndex
    Tap           accessibility_id=AccountIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT22/${Screenshot}22-04-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=ticket-confirmation
    Tap           accessibility_id=ticket-confirmation
    Sleep       3s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT22/${Screenshot}22-04-2.png
    Wait until element is visible           chain=**/XCUIElementTypeOther[`label == "billCoupon.item.1075"`][2]
    Tap                 chain=**/XCUIElementTypeOther[`label == "billCoupon.item.1075"`][2]
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "onDelete"`]
    Tap                  chain=**/XCUIElementTypeButton[`label == "onDelete"`]
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT22/${Screenshot}22-04-3.png
    Sleep       3s
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "ตกลง"`]
    Tap                  chain=**/XCUIElementTypeButton[`label == "ตกลง"`]
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT22/${Screenshot}22-04-4.png
    Sleep       3s


TCAT-23-01 Coupon free Display
    Wait until element is visible           accessibility_id=AccountIndex
    Tap           accessibility_id=AccountIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT23/${Screenshot}23-01-1.png
    Sleep       3s
    Wait until element is visible           accessibility_id=ticket
    Tap            accessibility_id=ticket
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT23/${Screenshot}23-01-2.png
    Sleep       2s
    Wait until element is visible           accessibility_id=coupon.item.${namecouponfree}
    Tap           accessibility_id=coupon.item.${namecouponfree}
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT23/${Screenshot}23-01-2.png
    Sleep       3s












TCAT-16-01 Coupon Display
    Wait until element is visible           accessibility_id=AccountIndex
    Tap           accessibility_id=AccountIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT16/${Screenshot}16-01-1.png
    Sleep       3s
    Wait until element is visible           id=ticket-confirmation-outline
    Tap           id=ticket-confirmation-outline
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT16/${Screenshot}16-01-2.png
    Sleep       2s
    Wait until element is visible           id=tab.availables
    Tap           id=tab.availables
    Sleep       2s
    Wait until element is visible           id=tab.useds
    Tap           id=tab.useds
    Sleep       2s
    Wait until element is visible           id=header-back
    Tap           id=header-back
    Sleep       3s



TCAT-17-01 Change Password 
    Wait until element is visible           accessibility_id=AccountIndex
    Tap           accessibility_id=AccountIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT17/${Screenshot}17-01-1.png
    Sleep       3s
    Wait until element is visible           id=key
    Tap           id=key
    Sleep       2s
    Wait until element is visible           id=password_current
    Input Text          id=password_current             123456
    Wait until element is visible           id=password
    Input Text          id=password               123456
    Wait until element is visible           id=password_confirmation
    Input Text          id=password_confirmation            123456
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT17/${Screenshot}17-01-2.png
    Sleep       2s
    Tap         id=submit
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT17/${Screenshot}17-01-3.png
    Sleep       3s
    



TCAT-18-01 Contact Display
    Wait until element is visible           accessibility_id=AccountIndex
    Tap           accessibility_id=AccountIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT18/${Screenshot}18-01-1.png
    Sleep       2s
    Wait until element is visible           id=bubbles
    Tap           id=bubbles
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT18/${Screenshot}18-01-2.png
    Sleep       2s
    Wait until element is visible           id=header-back
    Tap           id=header-back
    Sleep       3s


TCAT-19-01 Info Display
    Wait until element is visible           accessibility_id=AccountIndex
    Tap           accessibility_id=AccountIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT19/${Screenshot}19-01-1.png
    Sleep       2s
    Wait until element is visible           id=wallet
    Tap           id=wallet
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT19/${Screenshot}19-01-2.png
    Sleep       2s
    Wait until element is visible           id=header-back
    Tap           id=header-back
    Sleep       3s

TCAT-20-01 Terms of Service 
    Wait until element is visible           accessibility_id=AccountIndex
    Tap           accessibility_id=AccountIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT20/${Screenshot}20-01-1.png
    Sleep       2s
    Wait until element is visible           id=pin
    Tap           id=pin
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT20/${Screenshot}20-01-2.png
    Sleep       2s
    Wait until element is visible           id=header-back
    Tap           id=header-back
    Sleep       3s


TCAT-21-01 Log out
    Wait until element is visible           accessibility_id=AccountIndex
    Tap           accessibility_id=AccountIndex
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT21/${Screenshot}21-01-1.png
    Sleep       2s
    Wait until element is visible           id=log-out
    Tap           id=log-out
    Sleep       2s
    Tap           id=log-out
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT21/${Screenshot}21-01-2.png
    Wait until element is visible           chain=**/XCUIElementTypeButton[`label == "ตกลง"`]
    Tap           chain=**/XCUIElementTypeButton[`label == "ตกลง"`]
    Sleep       3s
    Capture Page Screenshot          /Users/praephat/Documents/GitHub/tcat-cargo-user-mobile-ios/Capture/Capture-TCAT21/${Screenshot}21-01-3.png

